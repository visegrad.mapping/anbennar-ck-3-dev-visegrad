﻿# Racial Succession Laws
title_racial_legitimacy_laws = {
	flag = racial_legitimacy_law
	elf_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = elf
			}
		}
		should_start_with = {
			is_race = { RACE = elf }
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	half_elf_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = half_elf
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	human_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = human
			}
		}
		should_start_with = {
			is_race = { RACE = human }
			culture = { NOT = { has_innovation = innovation_elvenization } }
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	human_plus_halves_only = {
		can_pass = {
			can_change_group_racial_law_trigger = {
				GROUP = part_human
			}
		}
		should_start_with = {
			OR = {
				AND = {
					is_race = { RACE = human }
					culture = { has_innovation = innovation_elvenization }
				}
				is_race = { RACE = half_elf }
			}
		}
		flag = racial_group_succession_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	dwarf_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = dwarf
			}
		}
		should_start_with = {
			is_race = { RACE = dwarf }
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	halfling_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = halfling
			}
		}
		should_start_with = {
			is_race = { RACE = halfling }
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	gnome_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = gnome
			}
		}
		should_start_with = {
			is_race = { RACE = gnome }
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	gnomeling_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = gnomeling
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	orc_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = orc
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	half_orc_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = half_orc
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	gnoll_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = gnoll
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	kobold_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = kobold
			}
		}
		should_start_with = {
			is_race = { RACE = kobold }
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	troll_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = troll
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	trollkin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = trollkin
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	ogre_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = ogre
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	ogrillon_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = ogrillon
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	goblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = goblin
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	half_goblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = half_goblin
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	hobgoblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = hobgoblin
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	half_hobgoblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = half_hobgoblin
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	lesser_hobgoblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = lesser_hobgoblin
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	harimari_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = harimari
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	centaur_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = centaur
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	harpy_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = harpy
			}
		}
		should_start_with = {
			always = no
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	all_civilized_only = {
		can_pass = {
			can_change_group_racial_law_trigger = {
				GROUP = civilized
			}
		}
		should_start_with = {
			always = no
		}
		flag = racial_group_succession_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	all_monstrous_only = {
		can_pass = {
			can_change_group_racial_law_trigger = {
				GROUP = monstrous
			}
		}
		should_start_with = {
			always = no
		}
		flag = racial_group_succession_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
}

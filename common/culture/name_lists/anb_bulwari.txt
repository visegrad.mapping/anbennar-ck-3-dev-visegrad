﻿name_list_bahari = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Aban Abazu Absul Adad Aghan Akamis_ Aktan Akur Alaris Ans_ar Arad Aragad Aras_k Areias Arfandar Arhab Arkam Ars_ak Ars_am Ars_awir Ars_en Arsafir Arsarun Artasak Arwand 
		As_an As_ur Astanis_ Asur Atbin Athan Awbal Baraz Bared Barid Barsam Barseen Bawar Behlil Bullud Dagan Darab Darian Darius_ Dartaxes Dikran Durul Duzan Duzar Duzi 
		Ekrem Erbayu Eril Ertenu Es_khan Fahagnas_ Fardanir Fazil Fresis_ Gakig Garegan Garnar Giz Guregh Haykasar Hazan Himmet Hrantak Huran Husaam Idad Idris Iraskar Irran Is_eles 
		Kahit Kalib Kaptan Kartal Kasbar Kaweh Kikud Kis_ag Kuridan Laris Larza Limer Manukar Marud Marudeen Mehran Mesten Mihran Mihrtad Misak Nahrun Naram Nawas_art Nersefan 
		Numair Panthes Paramas_ Parsagh Raafi Radahn Radin Rimus_ Saamir Sabum Saed Saghafan Samiun Sargin Sepeh Serim Serinar S_araf Sisakar Smbatan Suhlamu Surenik Tailmaz Tarenas_ Tazan 
		Therin Tigranas_ Udis_ Wahram Wanus_ Yazkur Yeghs_eh Zaid Zamman Zarehtas_ Zerafak Zimud Zimudar

		#Generic Bulwari. In case of adding new namelist, add these too. They are already integrated in the existing namelists.
		#Aban Abazu Adad Akur Ans_ar Arad Aras_k Arhab Ars_am Arwand As_an As_ur Atbin Athan Awbal Baraz Barid Barseen Bawar Behlil Bullud Dagan Darab Darian Darius_ Dartaxes
		#Durul Duzan Duzar Duzi Ekrem Erbayu Ertenu Fazil Giz Hazan Himmet Huran Husaam Idad Idris Irran Kahit Kalib Kaptan Kartal Kaweh Kikud 
		#Laris Larza Limer Mehran Mesten Naram Numair Panthes Raafi Radin Rimus_ Saamir Sabum Saed Samiun Sargin Sepeh Serim S_araf Suhlamu 
		#Udis_ Yazkur Zaid Zamman Zimud Zimudar
	}
	female_names = { 
		Aamina Abella Almastu Amata Amina Anahit Anaisa Anatu Anunia Aqeela Araha Arahuna Araxia Arefi Arekis Arusen Arwia Asiah Asmika Astaghik Aznif Baila Benan Kanfeda Damrina Derya 
		Dilara Elif Elina Elmas Emmita Ettu Fardit Fariana Fatma Gula Haala Haamida Hasibe Hasmika Hripset Ia Iltani Is_tara Kamelya Kis_ar Kullaa Lubna Marali Mardina Marusa Mudam 
		Mus_eera Nadide Nahrina Nairia Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Paranzel Pari Qamara Rahma S_us_anik Saba Sabahat Samra Sasema Satenis Semiha Seyma Sipanik Siranik Sireen 
		Ura Yamima Yamina Zabelis Zakiti Zeyni

		#Generic Bulwari. In case of adding new namelist, add these too. They are already integrated in the existing namelists.
		#Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Kanfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida Hasibe Ia Iltani Is_tara Kamelya
		#Kis_ar Kullaa Lubna Mardina Mudam Mus_eera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Pari Qamara Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_gelkar = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Aban Abazu Adad Akur Alzarka Ans_ar Arad Aras_k Arhab Ars_am Arwand As_an As_ur Atbin Athan Awbal Baraz Barid Barseen Bawar Behlil Bullud Dagan Darab Darian 
		Darius_ Dartaxes Durul Duzan Duzar Duzi Ekrem Erbayu Ertenu Fazil Giz Harkus_ Hattilis Hazan Himmet Huran Huranku Husaam Idad Idris Irran Is_kiya Istanar Kahit Kalib 
		Kaptan Kartal Kaweh Khatlanas Kikud Kunnar Kuppal Kurunta Laris Larza Limer Lullar Maruhtur Mehran Mesten Naram Nirukh Numair Panthes Parnili Parnuwan Pudalkan Raafi 
		Radin Rimus_ S_alpasar S_araf S_ulzis Saamir Sabum Saed Samiun Sargin Sepeh Serim Suhlamu Tarhunzal Tarkhamar Telipkan Tudhark Tukkaz Udis_ Uman Wars_ut Yazkur Zaid Zamman 
		Zankus_ Zimud Zimudar
	}
	female_names = { 
		Aamina Abella Alkati Amata Amina Anatu Aqeela Araha Arahuna Aras_ur Arwia As_ankiya Asiah Baila Benan Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
		Hasibe Hulziya Ia Iltani Is_kara Is_kurs_a Is_tara Kamelya Kanfeda Khatlina Kis_ar Kullaa Kullani Kurunza Kus_tali Lubna Mardina Mudam Mus_eera Nadide Nahrina Nardina Nesrin Ninkari 
		Ninsunu Nuriye Nusaiba Nuzha Pari Parnikala Parnuli Purniya Qamara Rahma S_alpari S_arnili S_uppiya Saba Sabahat Samra Semiha Seyma Sireen Suppiriya Tarhuna Tarkhuwiya Tas_iyanna Tas_ulmi 
		Tukkariya Ura Wars_ila Yamima Yamina Zakiti Zallis Zeyni
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_brasanni = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Aban Abazu Abdsakar Adad Adirian Akur Amalqar Ans_ar Arad Araha Aras_k Arhab Ars_am Arwand As_an As_dar As_ur Atbin Athan Awbal Balas_ir Balqadan Barakan Baraz Barid Barseen 
		Bawar Behlil Bullud Dagan Daganir Darab Darian Darius_ Dartaxes Durul Duzan Duzar Duzi Ekrem Elikar Elissastra Erbayu Ertenu Es_man Fazil Giz Hadrius Haldar Hanarim Hannastra 
		Hannir Hazan Himmet Huran Husaam Idad Idris Irran Is_kan Kahit Kaldarim Kalib Kalliman Kaptan Kartal Kaweh Kedman Kessarim Kikud Laris Larza Limer Maganir Malir Mehran Mekarun 
		Melkaz Melqasim Merzal Mesten Naram Nirban Numair Panthes Pharis Phelqas Qadris Qartarun Qetziram Raafi Radin Rimus_ S_araf S_arimar S_ethril Saamir Sabum Saed Samiun Sargin Sepeh 
		Serim Sidanar Suhlamu Tafris Tanizir Tanqumal Tars_isim Tufilim Tyrium Udis_ Yazkur Yis_ban Zaid Zalimar Zamman Zarqeth Zimqeth Zimud Zimudar
	}
	female_names = { 
		Aamina Abella Amata Amina Anatia Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Damaris Damrina Delilah Derya Dilara Elethea Eliara Elif Elina Elmas Emmita Ettu Fatma Gula Haala 
		Haamida Hadara Hagaris Hasibe Hazaria Ia Iltani Is_ara Is_tar Is_tara Kalila Kamelya Kanfeda Keturah Kis_ar Kullaa Lubna Maariyah Mardina Miriam Mudam Mus_eera Nadide Nahrina Nardina 
		Nesrin Ninsunu Nuriye Nusaiba Nuzha Pari Qamara Rahma S_aphira Saba Sabahat Samra Selima Semiha Seyma Sireen Talitha Thalassa Tzipparah Ura Yaela Yamima Yamina Zakiti Zarina Zephyra Zeyni Zifa
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_surani = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Aban Abazu Adad Adadu Akur Ans_ar Arad Aras_k Arhab Ars_am Arwand As_an As_kalun As_khan As_ur As_urak As_urdan As_urram Assurik Atbin Athan Awbal Baraz Barid Barseen Bawar Behlil 
		Belitak Belranis Bullud Dagan Darab Darian Darius_ Dartaxes Durias Durul Duzak Duzan Duzar Duzi Ekrem Enlilis Ens_ar Erbayu Ertenu Fazil Gilbarad Giz Gulkhan Hazan Himmet Huran Husaam 
		Idad Idris Ilakar Irran Is_tarai Is_tarnu Istarik Kahit Kalib Kaptan Kartal Kaweh Kikud Kuras_ Laris Larza Limer Mardint Mardukar Marzallu Mehran Mesten Nabanar Nabudan Nabukal Nabukar 
		Naburam Naram Naramus Nergalim Ninlilis Ninurim Nirras_ Nisannu Nudar Numair Panthes Raafi Radin Rakhan Rimus_ S_almanar S_amgar S_amuram S_amurik S_araf S_armansar Saamir Sabum Saed 
		Samas_i Samiun Sargin Semiz Sennakar Sepeh Serim Sinram Suhlamu Tiamukar Tis_ur Tugan Tukharn Tukultar Udis_ Uratim Urdin Yazkur Zabril Zaid Zamman Zimud Zimudar
	}
	female_names = { 
		Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia As_kura Asiah Baila Bels_ara Benan Damrina Derya Dilara Durana Durkana Elakana Elif Elina Elmas Emmita Ens_at Ettu Fatma Gilhana 
		Gula Haala Haamida Hasibe Ia Iltani Is_tara Is_urna Kamelya Kanfeda Karzana Khadira Khallia Kis_ar Kullaa Lubna Mardina Marzana Mudam Mus_eera Nadide Nahrina Nalara Nardina Nesrin Ninsunu 
		Nirissa Nuriye Nusaiba Nuzha Pari Qamara Rahma Ris_at S_alanda S_amira Saba Sabahat Samra Semiha Seyma Sireen Sumeri Tamaris Tuhina Ura Urrat Yamima Yamina Zabrina Zakiti Zaris_ Zeyni Zurna
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_zanite = {

	cadet_dynasty_names = {
		"dynn_ars_ati"
		"dynn_as_ayana"
		"dynn_yaxara"
		"dynn_kayara"
		"dynn_bawandar"
		"dynn_s_amanar"
		"dynn_baturkar"
		"dynn_saran"
		"dynn_wahran"
		"dynn_kahrendar"
		"dynn_tahmurar"
		"dynn_aryanas"
		"dynn_kaskandar"
		"dynn_dilanas"
		"dynn_adurianas"
	}

	dynasty_names = {
		"dynn_ars_ati"
		"dynn_as_ayana"
		"dynn_yaxara"
		"dynn_kayara"
		"dynn_bawandar"
		"dynn_s_amanar"
		"dynn_baturkar"
		"dynn_saran"
		"dynn_wahran"
		"dynn_kahrendar"
		"dynn_tahmurar"
		"dynn_aryanas"
		"dynn_kaskandar"
		"dynn_dilanas"
		"dynn_adurianas"
	}
	
	male_names = {
		Aban Abazu Adad Akur Andar Ans_ar Arad Aras_ Aras_k Ardalan Ardan Ardas_ Ardas_ir Arhab Ars_am Artax Arwand As_an As_ur Atbin Athan Awbal Azariel Bahar Baraz Barid Barseen Barzas 
		Bawar Behlil Bullud Dagan Darab Darian Darius_ Dartaxes Daryan Daryus Durul Duzan Duzar Duzi Ekrem Erbayu Ertenu Fahus_ Fardus_ Faribarz Fazil Fehan Fereydun Feridun Fis_taspa Giz 
		Gurdar Hazan Himmet Huran Hus_ang Husaam Idad Idris Irmin Irran Kafad Kahit Kalib Kaptan Kartal Karus_ Kaspar Kaweh Kawad Kherus_an Kikud Kyrus_ Laris Larza Limer Mardin Marzan Mehran Mesten 
		Mithras_ Naram Numair Nyras Panthes Parandis Parfiz Paryafar Pireez Raafi Radin Res_an Rexar Rimus_ S_afed S_araf Saamir Sabum Saed Saman Samiun Sams_id Sarfaz Sargin Sepeh Serakh Serim 
		Suhlamu Tafrin Takhmasp Tigran Turan Udes_ Udis_ Xerak Yazdan Yazkur Zaid Zalgar Zamman Zarhran Zelhan Zerak Zimud Zimudar
	}
	female_names = { 
		Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Aras_ Ardana Aruhana Arwia Asiah Aylara Azura Baila Benan Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Farana Farnus_ Farsa 
		Fatma Feres_t Gula Haala Haamida Hasibe Ia Iltani Is_tara Kamelya Kanfeda Kis_ar Kullaa Laleh Leyla Lubna Mardina Mars_an Mudam Mus_eera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye 
		Nusaiba Nuzha Nylara Pari Parissa Periza Qamara Rahma Rexan Rus_ana S_ahan S_irin Saba Sabahat Samira Samra Semiha Seyma Siran Sireen Suriya Tafara Tala Ura Yamima Yamina Yaranis Zakiti 
		Zarin Zeyni Zuhra
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_kuzarami = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Aban Abazu Adad Akur Ans_ar Arad Aras Aras_k Arazan Arfan Arhab Ars_am Arwand Aryan As_an As_ur Atbin Athan Awbal Bahman Bahram Baran Baraz Barid Barseen Barzan Bawar Behlil Berhak Bullud Dagan
		Dara Darab Darian Darius_ Dartaxes Derya Durul Duzan Duzar Duzi Ekrem Erbayu Ertenu Farhad Farukh Fazil Giz Gulan Hafan Hazan Himmet Huran Husaam Idad Idris Irran Kahit Kalib Kamal Kaptan Kartal 
		Karwan Kawa Kaweh Kikud Kufan Laleh Laris Larza Limer Lirak Marzand Mehran Mesten Nafruz Naram Narin Nis_tman Numair Nuri Panthes Payam Perfan Raafi Radin Raman Rengin Rezgar Rimus_ Rizgar Rus_an 
		Rus_in S_adman S_araf S_ifan Saamir Sabum Saed Samiun Sargin Sepeh Serim Serwan Suhlamu Tariq Tiryaq Udis_ Xandar Yazkur Zaid Zalgin Zamman Zanyar Zeraf Zhirayr Zilan Zimud Zimudar
	}
	female_names = { 
		Aamina Abella Afa Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Bahar Baila Banu Benan Berifan Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fariba Fatma Gula Haala Haamida Hafin Hasibe 
		Ia Iltani Is_tara Kamelya Kanfeda Kis_ar Kullaa Leila Lubna Mardina Melis Meryem Mudam Mus_eera Nadide Nahrina Nardina Nasrin Nesrin Ninsunu Nuriye Nusaiba Nuzha Parfaneh Pari Qamara Rahma Rinak 
		Rus_ina S_ahnaz S_ilan S_ireen S_irin S_ukhan Saba Sabahat Samra Semiha Seyma Sima Sireen Sirin Tara Ura Yamima Yamina Yasmin Zakiti Zara Zeyni Zilan Zuhra
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_maqeti = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Aban Abazu Adad Adarin Akur Ans_ar Arad Aras_k Ardus_ Arhab Ars_am Artex Arwand As_an As_der As_ur Atbin Athan Awbal Bahir Balqadin Barakin Baraz Barid Barseen Bawar Behlil Belas_ir 
		Bullud Dagan Darab Darian Darius_ Dartaxes Dasma Durul Duzan Duzar Duzi Ekrem Elissastra Erbayu Ertenu Es_men Fahis_ Fazil Fehin Fereydin Feribarz Feridin Firdus_ Giz Hannar Hannis_tra 
		Hazan Himmet Huran Husaam Idad Idris Irran Is_ken Kafid Kahit Kalib Kaptan Kartal Kaweh Kharus_in Kikud Kirus_ Laris Larza Limer Malqesim Maqet Marzin Mehran Mekarin Melkez Mesten Mithrus_ 
		Naram Numair Panthes Parfez Paryafir Phalqis Qertarin Qitzerim Raafi Radin Res_in Rexir Rimus_ S_araf S_eramar Saamir Sabum Sadenir Saed Samin Samiun Sarfes_ Sargin Sepeh Serim Suhlamu Tanqemil 
		Tefrin Tefris Tegrin Udis_ Urdas_ir Yazkur Yis_bin Zaid Zalgir Zalimir Zamman Zarqith Zelhen Zerek Zimud Zimudar
	}
	female_names = { 
		Aamina Abella Alira Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Azira Baila Benan Damrina Derya Dilara Elif Elina Elmas Emmita Enatia Ettu Eyala Fatma Ferana Firs_a Gula Haala Haamida 
		Has_eria Hasibe Hegaris_ Ia Iltani Is_era Is_tara Kamelya Kanfeda Ketirah Kis_ar Kullaa Layla Lubna Mardina Mers_an Mudam Mus_eera Nadide Nahrina Nardina Nelara Nesrin Ninsunu Nuriye Nusaiba 
		Nuzha Pari Piriza Qamara Rahma Rixan S_ehin S_elima S_erin S_ifa Saba Sabahat Samra Sariya Semiha Seyma Sireen Sirin Thalis_a Ura Yamima Yamina Zahra Zakiti Zerina Zeyni
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_masnsih = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Aban Abazu Adad Akur Ander Ans_ar Arad Aras_k Ardin Arhab Ars_am Arwand As_an As_erik As_ur As_urrim Atbin Athan Awbal Azeriel Balranis_ Baraz Barid Barseen Bawar Behlil 
		Bullud Dagan Darab Darian Darias_ Darius_ Dartaxes Durul Duzan Duzar Duzi Edadu Ekrem Enlelis_ Erbayu Erdalin Ertenu Es_tarik Es_terai Es_urdin Faridin Fazil Fes_tispa Gelkhan 
		Gerdir Gilberid Giz Has_eng Hazan Himmet Huran Husaam Idad Idris Ilekar Irran Kahit Kalib Kaptan Kartal Kaweh Kespir Kikud Laris Larza Limer Mehran Merdukar Merzallu Mesten Nadir 
		Naram Nebukar Nebukil Neburim Nerris_ Niras_ Numair Panthes Perandis_ Raafi Radin Res_in Rimus_ S_araf S_emgair S_emuram S_erfaz Saamir Sabum Saed Samiun Sargin Semas_i Sems_id Sepeh Serim 
		Suhlamu Tarin Tefrin Tekhirn Tekhmasp Udis_ Urratim Xirek Yazkur Yezdin Zabril Zaid Zamman Zarhrin Zimud Zimudar Zulhin
	}
	female_names = { 
		Aamina Abella Ailara Amata Amina Anatu Aqeela Araha Arahuna Ardina Arwia As_kira Asiah Baila Bels_ira Benan Damrina Derina Derkina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Feris_t 
		Fernis_ Gula Haala Haamida Hasibe Ia Iltani Is_irna Is_tara Kamelya Kanfeda Khedira Khelia Kis_ar Kullaa Lalih Lubna Mardina Mudam Mus_eera Nadide Nahrina Nardina Nelira Nesrin Ninsunu Nuriye 
		Nusaiba Nuzha Pari Peris_a Qamara Rahma Ras_ana S_elinda Saba Sabahat Samra Semara Semiha Seyma Sireen Tefira Tehina Tela Temeris_ Ura Yamima Yamina Yerenis Zakiti Zerin Zeris_ Zeyni
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}
name_list_sadnatu = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Aban Abazu Adad Aderian Akur Amalqir Ans_ar Arad Aras_k Arhab Ars_am Arwand As_an As_dir As_ur Atbin Athan Awbal Bales_ir Balqidan Barakin Baraz Barid Barseen Bawar Behlil Bullud Dagan Daginir 
		Darab Darian Darius_ Dartaxes Durul Duzan Duzar Duzi Ekrem Elikir Elissestra Erbayu Ertenu Fazil Giz Haldir Hanerim Hannar Hannistra Hazan Hedrius Himmet Huran Husaam Idad Idris Irran Kahit Kalderim 
		Kalib Kallimin Kaptan Kartal Kaweh Kedmin Kesserim Kikud Laris Larza Limer Malkiz Malqesim Meganir Mehran Mekarin Melir Merzil Mesten Naram Nirbin Numair Panthes Phalqis Phares Qartirun Qatziram Qidris 
		Raafi Radin Rimus_ S_araf S_arimir S_athril Saamir Sabum Saed Samiun Sargin Sepeh Serim Sidanir Suhlamu Tanizar Tanqemal Tarim Tefilim Tefris Ters_isim Udis_ Us_kan Us_man Yazkur Yis_bin Zaid Zamman 
		Zamqith Zarqath Zelimar Zimud Zimudar
	}
	female_names = { 
		Aamina Abella Aliara Amata Amina Anatu Anetia Aqeela Araha Arahuna Arwia Asiah Baila Benan Dalila Dameris Damrina Derya Dilara Elif Elina Elithea Elmas Emmita Ettu Fatma Gula Haala Haamida 
		Hadera Hageris Hasibe Hazeria Ia Iltani Is_era Is_tara Is_tir Kamelya Kanfeda Kelila Ketirah Kis_ar Kullaa Lubna Mardina Meriah Miriem Mudam Mus_eera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye 
		Nusaiba Nuzha Pari Qamara Rahma S_ephira Saba Sabahat Samra Semiha Seyma Sireen Sulima Telitha Thalessa Ura Yamima Yamina Yela Zakiti Zefa Zephira Zeyni Zipparah Zurina
	}

	dynasty_of_location_prefix = "dynnp_szel"
	always_use_patronym = yes
	patronym_prefix_male = "dynnpat_szal"
	patronym_prefix_female = "dynnpat_szal"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
	
	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

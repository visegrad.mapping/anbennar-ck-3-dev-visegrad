﻿dwarven_hold_attrition_mult = {
	decimals = 0
	color = bad
	percent = yes
}

dwarven_hold_cancel_negative_supply = {
	decimals = 0
}

dwarven_hold_advantage = {
	prefix = MOD_ADVANTAGE_PREFIX
	decimals = 0
}

dwarven_hold_min_combat_roll = {
	decimals = 0
}

dwarven_hold_max_combat_roll = {
	decimals = 0
}

dwarven_hold_development_growth = {
	prefix = MOD_DEVELOPMENT_PREFIX
	decimals = 1
}

dwarven_hold_development_growth_factor = {
	prefix = MOD_DEVELOPMENT_PREFIX
	decimals = 0
	percent = yes
}

dwarven_hold_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_hold_holding_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_hold_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

dwarven_hold_holding_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

dwarven_hold_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

dwarven_hold_holding_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

dwarven_hold_supply_limit = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
}

dwarven_hold_supply_limit_mult = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
	percent = yes
}

dwarven_hold_tax_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_hold_levy_size = {
	decimals = 0
	prefix = MOD_SOLDIER_PREFIX
	percent = yes
}

dwarven_hold_surface_attrition_mult = {
	decimals = 0
	color = bad
	percent = yes
}

dwarven_hold_surface_cancel_negative_supply = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
}

dwarven_hold_surface_advantage = {
	prefix = MOD_ADVANTAGE_PREFIX
	decimals = 0
}

dwarven_hold_surface_min_combat_roll = {
	decimals = 0
}

dwarven_hold_surface_max_combat_roll = {
	decimals = 0
}

dwarven_hold_surface_development_growth = {
	prefix = MOD_DEVELOPMENT_PREFIX
	decimals = 1
}

dwarven_hold_surface_development_growth_factor = {
	prefix = MOD_DEVELOPMENT_PREFIX
	decimals = 0
	percent = yes
}

dwarven_hold_surface_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_hold_surface_holding_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_hold_surface_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

dwarven_hold_surface_holding_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

dwarven_hold_surface_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

dwarven_hold_surface_holding_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

dwarven_hold_surface_supply_limit = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
}

dwarven_hold_surface_supply_limit_mult = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
	percent = yes
}

dwarven_hold_surface_tax_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_hold_surface_levy_size = {
	decimals = 0
	prefix = MOD_SOLDIER_PREFIX
	percent = yes
}

dwarven_road_attrition_mult = {
	decimals = 0
	color = bad
	percent = yes
}

dwarven_road_cancel_negative_supply = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
}

dwarven_road_advantage = {
	prefix = MOD_ADVANTAGE_PREFIX
	decimals = 0
}

dwarven_road_min_combat_roll = {
	decimals = 0
}

dwarven_road_max_combat_roll = {
	decimals = 0
}

dwarven_road_development_growth = {
	prefix = MOD_DEVELOPMENT_PREFIX
	decimals = 1
}

dwarven_road_development_growth_factor = {
	prefix = MOD_DEVELOPMENT_PREFIX
	decimals = 0
	percent = yes
}

dwarven_road_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_road_holding_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_road_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

dwarven_road_holding_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

dwarven_road_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

dwarven_road_holding_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

dwarven_road_supply_limit = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
}

dwarven_road_supply_limit_mult = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
	percent = yes
}

dwarven_road_tax_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

dwarven_road_levy_size = {
	decimals = 0
	prefix = MOD_SOLDIER_PREFIX
	percent = yes
}

cavern_attrition_mult = {
	decimals = 0
	color = bad
	percent = yes
}

cavern_cancel_negative_supply = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
}

cavern_advantage = {
	prefix = MOD_ADVANTAGE_PREFIX
	decimals = 0
}

cavern_min_combat_roll = {
	decimals = 0
}

cavern_max_combat_roll = {
	decimals = 0
}

cavern_development_growth = {
	prefix = MOD_DEVELOPMENT_PREFIX
	decimals = 1
}

cavern_development_growth_factor = {
	prefix = MOD_DEVELOPMENT_PREFIX
	decimals = 0
	percent = yes
}

cavern_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

cavern_holding_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

cavern_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

cavern_holding_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

cavern_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

cavern_holding_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

cavern_supply_limit = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
}

cavern_supply_limit_mult = {
	prefix = MOD_SUPPLY_PREFIX
	decimals = 0
	percent = yes
}

cavern_tax_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

cavern_levy_size = {
	decimals = 0
	prefix = MOD_SOLDIER_PREFIX
	percent = yes
}


dwarven_hold_provisions_use_mult = {
	decimals = 0
	prefix = MOD_PROVISIONS_PREFIX
	color = bad
	percent = yes
}

dwarven_hold_surface_provisions_use_mult = {
	decimals = 0
	prefix = MOD_PROVISIONS_PREFIX
	color = bad
	percent = yes
}

dwarven_road_provisions_use_mult = {
	decimals = 0
	prefix = MOD_PROVISIONS_PREFIX
	color = bad
	percent = yes
}

cavern_provisions_use_mult = {
	decimals = 0
	prefix = MOD_PROVISIONS_PREFIX
	color = bad
	percent = yes
}

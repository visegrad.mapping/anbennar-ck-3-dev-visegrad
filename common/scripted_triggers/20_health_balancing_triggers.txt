﻿# Anbennar: racial ages, TODO: use a better way then "age_X_racial_equivalent"

### HEALTH BALANCING TRIGGERS ###
# Along with the health balancing effects, these triggers are only used to get data output used for balancing diseases.


# For activating data output on the number of victims (total, dead and surviving) for contagious disease outbreaks. Combine with a run file like this to quickly collect data:

#every_independent_ruler = {
#	random_courtier = {
#		trigger_event = health.1011 #1011 is plague, 1010 is smallpox
#	}
#}


debug_activate_contagious_disease_outbreak_data_trigger = {
	always = no
}

is_very_young_character = {
	OR = {
		# age <= 20
		age <= root.age_20_racial_equivalent # Anbennar
		AND = {
			OR = {
				has_trait = whole_of_body
				has_trait = fecund
			}		
			# age <= 25
			age <= root.age_25_racial_equivalent # Anbennar
		}
	}		
}

is_young_character = {
	OR = {
		# age <= 30
		age <= root.age_30_racial_equivalent # Anbennar
		AND = {
			OR = {
				has_trait = whole_of_body
				has_trait = fecund
			}
			# age <= 35
			age <= root.age_35_racial_equivalent # Anbennar
		}
	}	
}

is_aging_character = {
	OR = {
		# age >= 40
		age >= root.age_40_racial_equivalent # Anbennar
		AND = {
			OR = {
				has_trait = whole_of_body
				has_trait = fecund
			}
			age >= 45
			age >= root.age_45_racial_equivalent # Anbennar
		}
	}	
}

is_old_character = {
	trigger_if = {
		limit = {
			is_male = yes
		}
		# age >= define:NCharacter|MALE_ELDERLY_AGE
		age >=  root.age_50_racial_equivalent # Anbennar: vanilla defines has 50
	}
	trigger_else = {
		# age >= define:NCharacter|FEMALE_ELDERLY_AGE
		age >=  root.age_50_racial_equivalent # Anbennar: vanilla defines has 50
	}
}
﻿#Special succession types
#If adding new types to the decision, use a trigger, otherwise they may break if the culture is split.

special_succession_witengamot_trigger = {
	culture = { has_cultural_parameter = witenagemot_succession_enabled }
}

special_succession_thing_trigger = {
	culture = { has_cultural_parameter = scandinavian_elective_enabled }
}

special_succession_tanistry_trigger = {
	# culture = {
		# OR = {
			# has_cultural_pillar = heritage_brythonic
			# has_cultural_pillar = heritage_goidelic
		# }
	# }
	# Anbennar
	always = no
}

special_succession_jirga_trigger = {
	culture = { has_cultural_parameter = tribal_elective_enabled }
}

historical_succession_access_single_heir_succession_law_trigger = {
	OR = {
		#Anbennar changed these to our own
		has_title = title:e_bulwar
		has_title = title:e_kheterata
		has_title = title:k_kheterata
		has_title = title:k_irsmahap # so kheterata doesn't lose it
		#Add any subsequent exceptions here.
	}
	NOT = { government_allows = administrative }
}

historical_succession_access_single_heir_succession_law_youngest_trigger = {
	NOT = { government_allows = administrative }
	OR = {
		#Anbennar changed these to our own
		has_title = title:e_bulwar
		has_title = title:e_kheterata
		has_title = title:k_kheterata
		has_title = title:k_irsmahap # so kheterata doesn't lose it
		#Add any subsequent exceptions here.
	}
}

historical_succession_access_single_heir_dynasty_house_trigger = {
	# NOT = { government_allows = administrative }
	# AND = {
		# has_title = title:d_bohemia
		# culture = { has_innovation = innovation_table_of_princes }
	# }
	# Anbennar
	always = no
}

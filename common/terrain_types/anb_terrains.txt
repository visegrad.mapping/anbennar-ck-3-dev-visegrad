﻿@provisions_cost_minimal = 25
@provisions_cost_light = 50
@provisions_cost_medium = 80
@provisions_cost_high = 100
@provisions_cost_extreme = 150

dwarven_hold = {
	color = { 255 198 0 }
	
	travel_danger_color = hsv { 0 0 0.392 }
	travel_danger_score = dwarven_hold_danger_value
	provision_cost = @provisions_cost_minimal

	province_modifier = {
		supply_limit_mult = 0.6
		development_growth_factor = 0.25
		travel_danger = dwarven_hold_danger_value
	}

	defender_combat_effects = {
		name = combat_dwarven_hold
		image = combat_dwarven_hold
		advantage = 12
	}
	
	movement_speed = 0.8
	combat_width = 0.8
	
	audio_parameter = 1.0
}

dwarven_hold_surface = {
	color = { 255 198 80 }
	
	travel_danger_color = hsv { 0 0 0.392 }
	travel_danger_score = dwarven_hold_surface_danger_value
	provision_cost = @provisions_cost_minimal

	province_modifier = {
		supply_limit_mult = 0.6
		development_growth_factor = 0.25
		travel_danger = dwarven_hold_danger_value
	}

	defender_combat_effects = {
		name = combat_dwarven_hold
		image = combat_dwarven_hold
		advantage = 12
	}
	
	movement_speed = 0.8
	combat_width = 0.8
	
	audio_parameter = 1.0
}

cavern = {
	color = { 0 0 0 }

	travel_danger_color = hsv { 0 0 0.392 }
	travel_danger_score = cavern_danger_value
	provision_cost = @provisions_cost_high

	province_modifier = {
		supply_limit_mult = -0.5
		development_growth_factor = -0.5
		travel_danger = cavern_danger_value
	}
	defender_combat_effects = {
		name = combat_cavern
		image = combat_cavern 
		advantage = 15
	}

	movement_speed = 0.5
	combat_width = 0.5

	audio_parameter = 1.0
}

dwarven_road = {
	color = { 36 42 75 }

	travel_danger_color = hsv { 0 0 0.392 }
	travel_danger_score = dwarven_road_danger_value
	provision_cost = @provisions_cost_light

	province_modifier = {
		supply_limit_mult = 0.0
		development_growth_factor = -0.2
		travel_danger = dwarven_road_danger_value
	}
	defender_combat_effects = {
		name = combat_dwarven_road
		image = combat_dwarven_road 
		advantage = 5
	}

	movement_speed = 1.0
	combat_width = 0.6

	audio_parameter = 1.0
}
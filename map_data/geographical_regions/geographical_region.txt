﻿# Geographical regions
# Regions can be declared with one or more of the following fields:
#	duchies = { }, takes duchy title names declared in landed_titles.txt
#	counties = { }, takes county title names declared in landed_titles.txt
#	provinces = { }, takes province id numbers declared in /history/provinces
#	regions = { }, a region can also include other regions, however the subregions needs to be declared before the parent region. 
#		E.g. If the region world_europe contains the region world_europe_west then world_europe_west needs to be declared as a region before (i.e. higher up in this file) world_europe.

###########################################################################
# World Regions
#	These groups are mutually exclusive on the same tier & should cover every part of the map
###########################################################################

# Cannor

world_cannor_gerudia = {
	duchies = {
		# Bjarnrik
		d_bjarnland d_bifrutja d_revrland d_haugrmyrr d_sarbann d_kaldrland d_alptborg d_sidaett d_ismark
		# Obrtrol #disabled until we have trolls
		# d_dalrfjall d_thurrsbol
		# Rimurhals
		d_drekkiskali d_olavslund d_jotunhamr
		# Urviksten
		d_esfjall d_naugsvol d_avnkaup
		# Jotuntar
		d_norjotuntar d_sudjotuntar
	}
}

world_cannor_west_lencenor = {
	duchies = {
		# Lorent
		d_lorentaine d_rosefield d_ainethan d_redglades d_rewanwood d_upper_bloodwine d_lower_bloodwine
		# Deranne
		d_deranne d_darom
		# Enteben
		d_enteben d_great_ording d_horsegarden d_crovania
		# Sorncost
		d_sorncost d_sormanni_hills d_coruan d_venail
		# Rubyhold
		d_rubyhold
		# Iochand
		d_iochand d_portnamm d_southroy
		# Tretun
		d_tretun d_roilsard
		
		d_viswall d_barrowshire d_greymill d_thomsbridge
		d_elkmarch
		d_roysfort d_bigwheat
		d_pearview d_appleton
		d_reaver_coast d_gnomish_pass
	}
}

world_cannor_west_dameshead = {
	duchies = {
		# Dameria
		d_damesear d_wesdam d_neckcliffe d_exwes d_istralore d_silverwoods d_acromton d_plumwall d_upper_luna d_heartlands
		# Carneter
		d_carneter
		# Pearlsedge
		d_pearlsedge d_pearlywine
		# Verne
		d_verne d_wyvernmark d_the_tail d_eastneck d_menibor_loop d_armanhal d_galeinn
		# Esmaria
		d_estallen d_konwell d_bennon d_leslinpar d_cann_esmar d_ryalanar d_songbarges d_asheniande d_hearthswood d_silverforge
		# Wex
		d_wexhills d_escandar d_bisan d_ottocam d_greater_bardswood d_sugamber
		# The Borders
		d_gisden d_brinkwick d_arannen d_celliande d_toarnen d_antirhal d_tellum d_hawkfields
		# Damescrown
		d_damescrown d_aranmas
		# Vertesk
		d_vertesk d_floodmarshes
		# Uelaire
		d_uelaire
		# Beepeck
		d_beepeck
		# Busilar
		d_lioncost d_busilari_straits d_lorin d_canno d_hortosare d_lorbet d_hapaine
		# Eborthil
		d_eborthil d_tefkora
	}
}

world_cannor_west_alenor = {
	duchies = {
		# Gawed
		d_gawed d_westmounts d_ginnfield d_alenic_expanse d_greatmarch d_balvord d_oudescker d_arbaran d_freecestir
		# Eaglecrest
		d_dragonhills d_fluddhill
		# Westmoors
		d_westmoor d_moorhills d_beronmoor
		# Adshaw
		d_adshaw d_bayvic d_west_chillsbay d_adderwood d_serpentgard d_celmaldor
		# Coddoran
		d_coddorran
		d_storm_isles
		
		d_crodam d_teagansfield
	}
}

world_cannor_west_dragon_coast = {
	duchies = {
		# Oddansbay
		d_oddansbay d_norcamb
		# Adderodd
		d_adderodd d_royoddan
		# Sildnamm
		d_sildnamm d_widdechand
		# Nimscodd 
		d_nimscodd
		# Mallazex 
		d_soxun_kobildzex d_kobildzex
	}
}

world_cannor_west = {
	regions = {
		world_cannor_west_lencenor world_cannor_west_dameshead world_cannor_west_alenor world_cannor_west_dragon_coast
	}
}

world_cannor_east_castanor = {
	duchies = {
		# Castonath
		d_castonath
		# Cast
		d_trialmount d_steelhyl d_serpentsmarck d_northyl d_westgate d_nath
		# Sarwick
		d_sarwood d_kondunn d_nortessord d_esshyl
		# Anor
		d_ardent_glade d_sapphirecrown d_nortmere d_oldhaven d_bradescker d_foarhal d_southgate
		# Marrhold
		d_marrhold d_dryadsdale d_doewood d_hornwood
		# Blademarches
		d_blademarch d_medirleigh d_swapstroke d_clovenwood d_beastgrave
		# Humacvord
		d_humacvord d_wallor d_themin d_burnoll
		# Devaced
		d_devaced d_vernham d_tulacarn
		# Adenica
		d_adenica d_rohibon d_verteben d_taran_plains d_acengard d_valefort
		# Merewood
		d_merescker d_nortmerewood d_oudmerewood
		# Balmire
		d_bal_mire d_falsemire d_middle_alen
		# Agrador
		d_cannwood d_agradalen
		# Vrorenmarch
		d_vrorenwall d_cedesck d_sondaar d_wudhal d_east_chillsbay d_ebonmarck d_tencfor
		# Ibevar
		d_ibevar d_nurael d_larthan d_whistlevale d_cursewood
	}
}

world_cannor_east_dostanor = {
	duchies = {
		# Corvuria
		d_bal_dostan d_blackwoods d_tiferben d_ravenhill
		# The Folly
		d_nathalaire d_daravan_folly
		# Ourdia
		d_tencmarck d_lencmarck d_oudmarck d_dostanmark
	}
}

world_cannor_deepwood = {
	duchies = {
		d_mountain_grove_1
		d_lake_grove_1
		d_bone_grove_1
		d_thorn_grove_1
		d_hunter_grove_1
		d_arrow_grove_1
		d_flower_grove_1
		d_river_grove_1
		d_shadow_grove_1
	}
}

world_cannor_east = {
	regions = {
		world_cannor_east_castanor world_cannor_east_dostanor world_cannor_deepwood
	}
}

world_cannor = {
	regions = {
		world_cannor_west world_cannor_east world_cannor_gerudia
	}
}

world_europe = { # for now
	regions = {
		world_cannor
	}
}

# Bulwar

world_sarhal_bulwar_north = {
	duchies = {
		# Re'uyel
		d_reuyel d_kisakur d_medbahar
		# Ovdal Tungr
		d_ovdal_tungr
		# Bahar
		d_aqatbahar d_zagnabad d_deskumar
		d_bahar_szel_uak
		# Gelkalis
		d_gelkalis d_lawassar d_elusadul
		# Harpylen
		d_heunthume d_tousavellen d_ayarallen d_hytiranyalen d_thissilen
		# Firanyalen
		d_firanyalen d_misarallen d_arkasul d_nansalen
	}
}

world_sarhal_bulwar_central = {
	duchies = {
		# Bulwar
		d_bulwar d_zanbar d_harklum d_kalib
		d_zansap d_nabasih d_sareyand
		# Imulušes
		d_imuluses d_sad_kuz
		# Idanas
		d_idanas d_anzabad d_medurubar d_ginerdu
		# Brasan
		d_sap_brasan d_baru_brasan d_len_brasan
		# Drolas
		d_arag_drolas d_baru_drolas d_annail
		# Sad Sur
		d_sad_sur d_agsilsu d_ekluzagnu d_ardutibad
		d_tumerub
		# Kumaršes
		d_kumarses d_gisshuram d_anzarzax
		# Akalšes
		d_akalses d_azkabar d_setadazar
		# Avamezan
		d_avamezan
		# Hašr
		d_hasr d_panuses d_eduz_vacyn
	}
}

world_sarhal_bulwar_east = {
	duchies = {
		# Azka Sur
		d_azka_sur d_jikarzax d_nabilsu
		# Seghdihr
		d_seghdihr
		d_seghrod
		# Siadanlen
		d_siadanlen d_baganas d_harnio
		d_mermigan d_siphefireri d_ebbusubtu
		# Verkal Gulan
		d_verkal_gulan
		d_heros_vale
		d_gulanrod
		# Apaših
		d_keruhar d_kesudsah d_saranza d_arsagnu
		# Hehodovar
		d_hehodovar
		d_hehorod
		# Gor Ozumbrog
		d_gor_ozumbrog
		d_xazorod
		d_ozumrod
	}
}

world_sarhal_bulwar = {
	regions = {
		world_sarhal_bulwar_north world_sarhal_bulwar_central world_sarhal_bulwar_east
	}
}

world_sarhal_salahad_east = {
	duchies = {
		# Harragrar
		d_harra d_ardu d_krahway d_gnolltauz
		# Krahwix
		d_krahilzin d_saymas d_koggraffa
		# Dasmazar
		d_udamsah d_nolhdrus d_dasmasih
	}
}

world_sarhal_salahad_akasik = {
	duchies = {
		# Ekha
		d_ekha d_akarat
		# Khasa
		d_khasa_anb d_ikasakan
		# Deshak
		d_deshak d_hapak
		# Ayshanaz
		d_krahkeysa d_axakmoz d_boozazn
	}
}

world_sarhal_salahad_kheterata = {
	duchies = {
		# Kheterata
		d_kheterat d_golkora d_awaashesh
		d_masusopot d_ibtat_axast d_ibtatu
		d_nirat d_hitputtiushesh d_anarat
		d_aakheta d_sopotremit d_ohitsopot
		# Irsmahap
		d_dawimshesh d_hapmot d_miugesh
	}
}

world_sarhal_salahad = {
	regions = {
		world_sarhal_salahad_kheterata world_sarhal_salahad_akasik world_sarhal_salahad_east
	}
}

world_sarhal = {
	regions = {
		world_sarhal_bulwar world_sarhal_salahad
	}
}

###########################################################################
# Material Regions
###########################################################################

 # Widespread Regional - Elm (Found all over Europe except southern Spain, Italy, and Greece)
material_wood_elm = {
	# regions = {
		# world_europe_west_britannia
		# world_europe_west_germania
		# world_europe_west_francia
		# world_europe_north
		# world_europe_south_east
		# world_europe_east
		# world_himalaya
	# }
	# duchies = {
		# #Punjab
		# d_multan d_lahore d_gandhara
		# #Delhi
		# d_kuru d_haritanaka d_mathura d_vodamayutja
		# #Kosala
		# d_kanyakubja d_saryupara d_jejakabhukti
	# }
	duchies = {
		d_test_1
	}
}

 # Widespread Regional - Walnut (found all over Europe excluding Scotland and Scandinavia, but including Northern Anatolia/Persia)
material_wood_walnut = {
	# regions = {
		# world_europe_west_germania
		# world_europe_west_iberia
		# world_europe_south
		# world_europe_east
		# world_middle_east_persia
		# world_asia_minor
		# world_himalaya
		# world_europe_west_francia
	# }
	# duchies = {
		# # England
		# d_bedford d_northumberland d_lancaster d_york d_norfolk d_hereford d_gloucester d_canterbury d_somerset
		# # Wales
		# d_gwynedd d_powys d_deheubarth d_cornwall
	# }
	
	duchies = {
		d_test_1
	}
}

 # Widespread Regional - Maple (found all over Europe excluding Scotland and northern Scandinavia, as well as northern Morocco/Tunisia/Persia and all of Anatolia)
material_wood_maple = {
	# regions = {
		# world_europe_west_germania
		# world_europe_west_iberia
		# world_europe_south
		# world_europe_east
		# world_asia_minor
		# world_transoxiana
		# world_himalaya
	# }
	# duchies = {
		# # England
		# d_bedford d_northumberland d_lancaster d_york d_norfolk d_hereford d_gloucester d_canterbury d_somerset
		# # Wales
		# d_gwynedd d_powys d_deheubarth d_cornwall
		# #Eastern Khorasan
		# d_merv d_herat d_ghur d_balkh
		# #Kabulistan
		# d_kabul d_zabulistan
		# #Zaporizhia
		# d_yedisan d_levedia d_don_valley d_crimea
		# #Caucasus
		# d_khazaria d_azov d_alania d_ciscaucasia
		# #Punjab
		# d_multan d_lahore d_gandhara
		# #Delhi
		# d_kuru d_haritanaka d_mathura d_vodamayutja
		# #Kosala
		# d_kanyakubja d_saryupara d_jejakabhukti
	# }
	
	duchies = {
		d_test_1
	}
}

# Widespread Regional - Pines and Firs (found all over continental Europe excluding northern France/Germany and the Balkans, but exists in Scotland and spreads East through Anatolia, Scandinavia, Russia, China, and Tibet)
material_woods_pine_and_fir = {
	# regions = {
		# world_europe_west_iberia
		# world_europe_north
		# world_europe_south
		# world_europe_east
		# world_asia_minor
		# world_steppe_tarim
		# world_steppe_west
		# world_steppe_east
		# world_europe_west_francia
	# }
	# duchies = {
		# d_western_isles d_galloway
		# d_bavaria d_nordgau d_salzburg d_augsburg d_steyermark d_osterreich d_tyrol d_carinthia
		# d_bohemia d_moravia
	# }
	
	duchies = {
		d_test_1
	}
}

# Widespread Regional - Subsaharan Africa
material_woods_subsaharan = {
	# regions = {
		# world_africa_west
		# world_africa_east
		# world_africa_sahara
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_padauk = {
	# regions = {
		# material_woods_subsaharan
		# world_india
		# world_burma
	# }
	
	duchies = {
		d_test_1
	}
}

# Widespread Regional - India
material_woods_india = {
	# regions = {
		# world_india_deccan
		# world_india_bengal
		# world_india_rajastan
	# }
	# duchies = {
		# d_arakan
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_india_burma = {
	# regions = {
		# world_india
		# world_burma
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_ebony = {
	# regions = {
		# material_woods_sri_lanka
	# }
	# duchies = {
		# d_igboland d_benin d_ife d_oyo d_nupe d_kwararafa
	# }
	
	duchies = {
		d_test_1
	}
}

# Regional
material_woods_yew = {
	# regions = {
		# world_europe_south_east
	# }
	# duchies = {
		# # England
		# d_bedford d_northumberland d_lancaster d_york d_norfolk d_hereford d_gloucester d_canterbury d_somerset
		# # Wales
		# d_gwynedd d_powys d_deheubarth d_cornwall
		# #Scandinavia
		# d_skane d_smaland d_viken d_agder d_vestlandi d_svealand
		# #Poland
		# d_mazovia d_masuria d_prussia d_kuyavia d_wielkopolska d_pomerelia
		# d_upper_silesia d_lower_silesia d_lesser_poland

		# #Alps
		# d_provence d_savoie d_piedmonte d_genoa d_transjurania d_currezia
		# d_tyrol d_carinthia d_salzburg

		# #Caucasia
		# d_abkhazia d_tao-klarjeti d_georgia d_antioch
	# }
	
	duchies = {
		d_test_1
	}
}

# EP2 Bow Woods
material_woods_bamboo = {
	# regions = {
		# world_burma
		# world_india
		# world_tibet
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_cherry = {
	# regions = {
		# world_burma
		# world_india
		# world_tibet
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_dogwood = {
	# regions = {
		# world_europe
		# world_asia_minor
		# world_steppe_west
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_hazel = {
	# regions = {
		# world_europe
		# world_asia_minor
		# world_steppe_west
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_hickory = {
	# regions = {
		# world_burma
		# world_steppe_east
		# world_tibet
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_palm = {
	# regions = {
		# world_africa_north
		# world_asia_minor
		# world_middle_east
	# }
	
	duchies = {
		d_test_1
	}
}

material_woods_mulberry = {
	# regions = {
		# world_africa_north
		# world_asia_minor
		# world_europe_south
		# world_india
		# world_middle_east
	# }
	
	duchies = {
		d_test_1
	}
}

# Regional - Mediterranean
material_woods_mediterranean = {
	# regions = {
		# world_africa_north
		# world_europe_west_iberia
		# world_europe_south
		# world_asia_minor
		# world_middle_east_jerusalem
	# }
	# duchies = {
		# d_provence d_languedoc d_sinai
	# }
	
	duchies = {
		d_test_1
	}
}

# Regional - Sri Lanka
material_woods_sri_lanka = {
	# regions = {
		# world_india_deccan
	# }
	
	duchies = {
		d_test_1
	}
}


material_cloth_no_silk = {
	# regions = {
		# world_africa_west
		# world_africa_east
	# }
	
	duchies = {
		d_test_1
	}
}

material_cloth_linen = {
	# regions = {
		# world_europe_west
		# world_europe_north
		# world_europe_south
		# world_europe_east
	# }
	
	duchies = {
		d_test_1
	}
}

material_cloth_cotton = {
	# regions = {
		# world_africa_north_east
		# world_africa_east
	# }
	
	duchies = {
		d_test_1
	}
}

material_metal_wootz = {
	# regions = {
		# world_india_deccan
		# world_india_bengal
		# world_india_rajastan
		# world_burma
	# }
	
	duchies = {
		d_test_1
	}
}

material_metal_damascus = {
	# regions = {
		# world_middle_east
		# world_asia_minor
		# world_africa_north_east
	# }
	
	duchies = {
		d_test_1
	}
}

material_metal_bulat = {
	# regions = {
		# world_europe_east
		# world_steppe_tarim
		# world_steppe_west
		# world_steppe_east
	# }
	
	duchies = {
		d_test_1
	}
}

material_hsb_camel_bone = {
	# regions = {
		# world_middle_east
		# world_africa_north
		# world_africa_west
		# world_africa_east
		# world_africa_sahara
		# world_india_rajastan
		# world_steppe_tarim
	# }
	
	duchies = {
		d_test_1
	}
}

material_hsb_deer_antler = {
	# regions = {
		# world_europe_west
		# world_europe_north
		# world_europe_south
		# world_europe_east
		# world_asia_minor
		# world_middle_east_jerusalem
		# world_middle_east_persia
		# world_india_deccan
		# world_india_bengal
		# world_india_rajastan
		# world_steppe_tarim
		# world_steppe_west
		# world_steppe_east
		# world_tibet
		# world_burma
		# world_mesopotamia
		# world_jazira
	# }
	# duchies = {
		# d_kermanshah
	# }
	
	duchies = {
		d_test_1
	}
}

material_hsb_boar_tusk = {
	# regions = {
		# world_europe_west
		# world_europe_north
		# world_europe_south
		# world_europe_east
		# world_asia_minor
		# world_middle_east_jerusalem
		# world_middle_east_persia
		# world_india_deccan
		# world_india_bengal
		# world_india_rajastan
		# world_steppe_west
		# world_steppe_east
		# world_africa_north
		# world_steppe_tarim
		# world_burma
		# world_mesopotamia
		# world_jazira
	# }
	# duchies = {
		# d_nobatia d_makuria d_shamir d_alwa d_sennar d_naqis d_sinai d_kermanshah
	# }
	
	duchies = {
		d_test_1
	}
}

material_hsb_ivory_imported = {
	# regions = {
		# world_europe_west
		# world_europe_south
		# world_asia_minor
		# world_middle_east
		# world_steppe_west
		# world_steppe_east
		# world_steppe_tarim
		# world_africa_north
		# world_tibet
	# }
	
	duchies = {
		d_test_1
	}
}

material_hsb_ivory_native = {
	# regions = {
		# world_india_deccan
		# world_india_bengal
		# world_india_rajastan
		# world_africa_west
		# world_africa_east
		# world_burma
	# }
	
	duchies = {
		d_test_1
	}
}

material_hsb_mother_of_pearl = {
	# regions = {
		# world_africa_north
		# world_europe_south_italy
		# world_india_deccan
		# world_burma
		# world_middle_east_arabia
		# world_africa_east
		# world_europe_west_iberia
	# }
	
	duchies = {
		d_test_1
	}
}

material_hsb_tortoiseshell = {
	# regions = {
		# world_africa_north
		# world_europe_south_italy
		# world_asia_minor
	# }
	# duchies = {
		# d_thrace d_strymon d_thessalonika d_thessaly
		# d_epirus d_dyrrachion d_cephalonia
		# d_athens d_achaia d_cyprus d_mallorca d_krete
	# }
	
	duchies = {
		d_test_1
	}
}

material_hsb_seashell = {
	# regions = {
		# world_africa_north
		# world_africa_east
		# world_europe_south_italy
		# world_europe_west_iberia
		# world_europe_west_francia
		# world_burma
		# world_middle_east
		# world_india
		# world_asia_minor
	# }
	# duchies = {
		# d_thrace d_strymon d_thessalonika d_thessaly
		# d_epirus d_dyrrachion d_cephalonia
		# d_athens d_achaia d_cyprus d_krete
	# }
	
	duchies = {
		d_test_1
	}
}


###########################################################################
# Custom Regions
###########################################################################

custom_inner_castanor = {
	duchies = {
		#Cast
		d_trialmount d_steelhyl d_serpentsmarck d_northyl d_westgate d_nath 
		#Anor
		d_ardent_glade d_sapphirecrown d_nortmere d_oldhaven d_bradescker d_foarhal d_southgate 
		#Castonath
		d_castonath
	}
}

custom_alenic_reach = {
	duchies = {
		#Adshaw
		d_serpentgard d_celmaldor d_adderwood d_adshaw d_bayvic d_west_chillsbay 
		#Vrorenmarch
		d_east_chillsbay d_sondaar d_wudhal d_cedesck d_vrorenwall 
	}
}

custom_arbaran = {
	duchies = {
		d_arbaran
		d_freecestir
		d_crodam
		d_teagansfield
	}
}

custom_westmoors = {
	duchies = {
		d_westmoor
		d_moorhills
		d_beronmoor
	}
}

custom_small_country = {
	duchies = {
		# Viswall
		d_viswall d_barrowshire d_greymill d_thomsbridge
		#Elkmarch
		d_elkmarch
		# Roysfort
		d_roysfort d_bigwheat
		# Pearlview
		d_pearview d_appleton
		d_beepeck
	}
}

custom_busilar = {
	duchies = {
		d_lioncost d_busilari_straits d_lorin d_canno d_hortosare d_lorbet d_hapaine
	}
}

custom_tef = {
	duchies = {
		d_eborthil d_tefkora
	}
}

custom_businor = {
	regions = {
		custom_busilar custom_tef
	}
}

custom_verne = {
	duchies = {
		d_verne d_wyvernmark d_the_tail d_eastneck d_menibor_loop d_armanhal d_galeinn
	}
}

custom_trystans_kingdom = {
	duchies = {
		d_burnoll d_humacvord d_medirleigh d_wallor d_themin
	}
}

custom_humacfeld = {
	generate_modifiers = yes
	duchies = {
		d_burnoll d_humacvord d_wallor d_themin
	}
}

###########################################################################
# Graphical Regions
###########################################################################

# Anbennar: decide on these

graphical_western = {
	graphical = yes
	color = { 255 0 0 }
	regions = {
		world_cannor_gerudia world_cannor_west_alenor world_cannor_west_dragon_coast world_cannor_east_castanor 
	}
	duchies = { # all this shit should be in regions, custom or not
		d_upper_luna
		
		d_lorentaine d_rosefield d_redglades d_rewanwood
		
		d_rubyhold
		d_iochand d_portnamm d_southroy
		
		d_viswall d_barrowshire d_greymill d_thomsbridge
		d_elkmarch
		d_roysfort d_bigwheat
		d_pearview d_appleton
		d_reaver_coast d_gnomish_pass
		
		d_estallen d_bennon d_leslinpar d_asheniande d_hearthswood d_silverforge
		d_wexhills d_escandar d_bisan d_ottocam d_greater_bardswood d_sugamber
		d_gisden d_brinkwick d_arannen d_antirhal d_tellum
		
		d_bal_dostan d_blackwoods d_tiferben d_ravenhill
		d_daravan_folly
	}
}

graphical_mena = {
	graphical = yes
	color = { 255 255 0 }
	regions = {
		world_sarhal
	}
}

graphical_india = {
	graphical = yes
	color = { 0 255 0 }
	# regions = {
		# world_india world_tibet world_burma
	# }
	duchies = { # placeholder until we add Rahen (for the log)
		d_test_1
	}
}

graphical_mediterranean = {
	graphical = yes
	color = { 0 0 255 }
	duchies = { # all this shit should be in regions, custom or not
		d_konwell d_cann_esmar d_ryalanar d_songbarges
		d_damesear d_wesdam d_neckcliffe d_exwes d_istralore d_silverwoods d_acromton d_plumwall d_heartlands
		d_carneter
		d_pearlsedge d_pearlywine
		d_verne d_wyvernmark d_the_tail d_eastneck d_menibor_loop d_armanhal d_galeinn
		d_damescrown d_aranmas
		d_vertesk d_floodmarshes
		d_uelaire
		d_beepeck
		d_lioncost d_busilari_straits d_lorin d_canno d_hortosare d_lorbet d_hapaine
		d_eborthil d_tefkora
		
		d_tretun d_roilsard
		d_ainethan d_upper_bloodwine d_lower_bloodwine
		d_sorncost d_sormanni_hills d_coruan d_venail
		d_enteben d_great_ording d_horsegarden d_crovania
		d_deranne d_darom
		
		d_tencmarck d_lencmarck d_oudmarck d_dostanmark
		d_nathalaire
		
		d_celliande d_toarnen d_hawkfields
	}
}

graphical_steppe = {
	graphical = yes
	color = { 0 255 255 }
	# regions = {
		# world_steppe
	# }
	duchies = { # placeholder
		d_khugdihr
	}
}

##############################
# Misc
###############################

world_innovation_elephants = {
	generate_modifiers = yes
	duchies = { # until stuff is made better
		# Re'uyel
		d_reuyel d_kisakur d_medbahar
		# Bahar
		d_aqatbahar d_zagnabad d_deskumar
		d_bahar_szel_uak d_nabahar
	}
}

world_innovation_camels = {
	generate_modifiers = yes
	regions = {
		world_sarhal
	}
}

world_horse_buildings_in_hills_and_mountains = {
	regions = {
		world_sarhal_bulwar world_sarhal_salahad # idk
	}
}

#####################
# Special Regions
#####################

#####################
# FP1 Regions
#####################

dlc_fp1_region_core_mainland_scandinavia = {
	regions = {
		world_cannor_gerudia
	}
}

### END FP1 Regions

divergence_island_regions = {
	duchies = {
		d_venail
		d_damesear
		d_tefkora
		d_eborthil
		d_nimscodd
		d_ibtatu
		d_jotunhamr
		d_bifrutja
		d_storm_isles
	}
	counties = {
		c_gemisle
		c_nathalaire
		c_the_pearls
		c_stingport
		c_bus_akanu
		c_bhetu
		c_pir_ail
		c_girakub
		c_annail
	}
}

### SEAZONES ###

# TODO

### Hunts

# Deer Spread
hunt_animal_deer_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

# Antelope Spread
hunt_animal_antelope_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

# Gazelle Spread
hunt_animal_gazelle_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

# Boar Spread
hunt_animal_boar_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

# Bear Spread
hunt_animal_bear_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

# Big Cat Spread
hunt_animal_big_cat_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

# Bison Spread
hunt_animal_bison_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

# Aurochs Spread
hunt_animal_aurochs_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

hunt_animal_reindeer_region = {
	duchies = {
		d_test_1 # placeholder
	}
}

hunt_animal_elk_region = {
	duchies = {
		d_test_1 # placeholder
	}
}
#test region
anb_has_minority_region = {
	counties = {
		c_aqatbar
	}
}
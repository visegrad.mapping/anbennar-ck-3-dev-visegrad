﻿askeling_0001 = { #King Daghr of Reveria
	name = "Daghr"
	dna = 49_daghr_askeling
	dynasty = dynasty_askeling #Askeling
	religion = "reverian_cult"
	culture = "reverian"
	
	trait = race_human
	trait = education_diplomacy_3
	trait = zealous
	trait = honest
	trait = patient
	trait = depressed_1

	father = askeling_0101
	mother = fuglborg_0101

	970.4.9 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	988.1.1 = {
		add_spouse = deranne_0016
	}
}

askeling_0002 = { #dead Prince Tomar of Reveria
	name = "Tomar"
	dynasty = dynasty_askeling #Askeling
	religion = "reverian_cult"
	culture = "reverian"
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = honest
	trait = impatient

	father = askeling_0001
	#mother = deranne_0016

	992.2.27 = {
		birth = yes
	}

	1021.7.19 = {
		death = {
			death_reason = death_wounded_1
		}
	}
}

askeling_0003 = { #Prince Edvard, heir to Reveria, twin of Ardur
	name = "Edvard"
	dynasty = dynasty_askeling #Askeling
	religion = "reverian_cult"
	culture = "reverian"
	
	trait = race_human
	trait = twin
	trait = bossy

	father = askeling_0001
	mother = deranne_0016

	1010.1.16 = {
		birth = yes
	}

}

askeling_0004 = { # Prince Ardur, twin of Edvard
	name = "Ardur"
	dynasty = dynasty_askeling #Askeling
	religion = "reverian_cult"
	culture = "reverian"
	
	trait = race_human
	trait = twin
	trait = rowdy

	father = askeling_0001
	mother = deranne_0016

	1010.1.16 = {
		birth = yes
	}

}

askeling_0005 = { #Princess Elisabet
	name = "Elisabet"
	dynasty = dynasty_askeling #Askeling
	religion = "reverian_cult"
	culture = "reverian"
	female = yes

	trait = race_human

	father = askeling_0001
	mother = deranne_0016

	1020.10.1 = {
		birth = yes
	}

}

askeling_0006 = { #Baron Sekill of Manyburrows
	name = "Sekill"
	dynasty = dynasty_askeling #Askeling
	religion = "reverian_cult"
	culture = "reverian"
	
	trait = race_human
	trait = education_diplomacy_3
	trait = cynical
	trait = content
	trait = gregarious
	trait = journaller

	father = askeling_0101
	mother = fuglborg_0101


	980.4.9 = {
		birth = yes
		add_pressed_claim = title:c_reavers_landing
		add_pressed_claim = title:k_reveria

	}
}

askeling_0101 = { #dead King Alvor "The Blind"
	name = "Alvor"
	dynasty = dynasty_askeling #Askeling
	religion = "reverian_cult"
	culture = "reverian"
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = diligent
	trait = arrogant
	trait = one_eyed

	father = askeling_0102
	mother = fuglborg_0102

	949.1.17 = {
		birth = yes
		give_nickname = nick_the_blind
	}

	967.1.1 = {
		add_spouse = fuglborg_0101
	}

	996.4.23 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

askeling_0102 = { #dead King Vilhelm II
	name = "Vilhelm"
	dynasty = dynasty_askeling #Askeling
	religion = "reverian_cult"
	culture = "reverian"
	
	trait = race_human
	trait = education_intrigue_2
	trait = stubborn
	trait = deceitful
	trait = calm

	father = askeling_0103

	915.5.11 = {
		birth = yes
	}

	933.1.1 = {
		add_spouse = fuglborg_0102
	}

	977.11.2 = {
		death = {
			death_reason = death_murder
		}
	}
}

askeling_0103 = { #dead King Vilhelm I
	name = "Vilhelm"
	dynasty = dynasty_askeling #Askeling
	religion = "skaldhyrric_faith"
	culture = "reverian"
	
	trait = race_human
	trait = education_martial_1
	trait = gluttonous
	trait = arbitrary
	trait = craven

	father = askeling_0104

	871.3.7 = {
		birth = yes
	}

	926.3.27 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

askeling_0104 = { #dead King Lokian
	name = "Lokian"
	dynasty = dynasty_askeling #Askeling
	religion = "skaldhyrric_faith"
	culture = "dalric"
	
	trait = race_human
	trait = education_diplomacy_3
	trait = patient
	trait = deceitful
	trait = fickle

	father = askeling_0105

	832.5.17 = {
		birth = yes
	}

	903.3.27 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

askeling_0105 = { #dead Askel Reaverking, founder of Reveria
	name = "Askel"
	dynasty = dynasty_askeling #Askeling
	religion = "skaldhyrric_faith"
	culture = "dalric"
	
	trait = race_human
	trait = education_martial_4
	trait = ambitious
	trait = just
	trait = stubborn
	trait = reaver


	791.10.10 = {
		birth = yes
		give_nickname = nick_reaverking
	}

	861.3.2 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}
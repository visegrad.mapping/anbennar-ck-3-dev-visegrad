#k_ginerdu
##d_idanas
###c_idanas
594 = {		#Idanas

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

6532 = {		#Tuzamu

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

6533 = {		#Kandoriq

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

###c_irrliam
595 = {		#Irrliam

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
	holding = castle_holding

    # History
}

6534 = {		#Eduz-Dazin

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
	holding = church_holding

    # History
}

6535 = {		#Anhu

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
	holding = none

    # History
}

6536 = {		#Obadel

    # Misc
    culture = sun_elvish
    religion = jaherian_cults
	holding = city_holding

    # History
}

###c_asr_limes
593 = {		#Asr Limes

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = castle_holding

    # History
}

6530 = {		#Rakasud

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = none

    # History
}

6531 = {		#Kusitan

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = city_holding

    # History
}

##d_anzabad
###c_anzabad
560 = {		#Anzabad

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = city_holding

    # History
}

6518 = {		#Zulmures

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = church_holding

    # History
}

6519 = {		#Qarlubad

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = city_holding

    # History
}

6520 = {		#Azka-Aleric

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = castle_holding

    # History
}

6521 = {		#Qinnazklum

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = none

    # History
}

###c_zanakes
559 = {		#Zanakes

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = castle_holding

    # History
}

6522 = {		#Takasankar

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = city_holding

    # History
}

6523 = {		#Halise

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = none

    # History
}

6524 = {		#Lim-qar-garklum

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = church_holding

    # History
}

###c_ginerdu
586 = {		#Ginerdu

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

6525 = {		#Hasaklum

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

6526 = {		#Nahirud

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

6527 = {		#Barkabar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

###c_duklum_tanuz
588 = {		#Betduhuras

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = city_holding

    # History
}

6528 = {		#Kurzan

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = none

    # History
}

6529 = {		#Jaharran

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = castle_holding

    # History
}

##d_medurubar
###c_medurubar
561 = {		#Medurubar

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = city_holding

    # History
}

6509 = {		#Heskalum

    # Misc
    culture = brasanni
    religion = cult_of_mourning
	holding = church_holding

    # History
}

6510 = {		#Irirhar

    # Misc
    culture = brasanni
    religion = cult_of_mourning
	holding = city_holding

    # History
}

6511 = {		#Barhamar

    # Misc
    culture = brasanni
    religion = cult_of_mourning
	holding = none

    # History
}

6512 = {		#Azka-Meduru

    # Misc
    culture = brasanni
    religion = cult_of_mourning
	holding = castle_holding

    # History
}

###c_aklum
584 = {		#Aklum

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = castle_holding

    # History
}

6513 = {		#Elikhersubtu

    # Misc
    culture = brasanni
    religion = cult_of_the_stone
	holding = none

    # History
}

6514 = {		#Zigkalum

    # Misc
    culture = brasanni
    religion = cult_of_the_stone
	holding = city_holding

    # History
}
﻿#k_castonath
##d_castonath
###c_north_castonath
2512 = {	#Imperial Quarter

    # Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

    # History

}
833 = {		#Upper District

	# Misc
	holding = city_holding

	# History
}
2513 = {	#Pantheonway

    # Misc
    holding = church_holding

    # History

}

###c_south_castonath
831 = {		#Dragonforge Hill

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = church_holding

	# History
	1000.1.1 = {
		special_building_slot = dragonforge_01
		special_building = dragonforge_01
	}
}
2514 = {	#The Crags

    # Misc
    holding = city_holding	#So im essentially making this place all non castles to force you to give them away. you can only rule from the imperial quarter. just something to explore

    # History

}
2515 = {	#Legion Square

    # Misc
    holding = city_holding

    # History

}

###c_lower_castonath
2511 = {	#Old Bazaar

    # Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = city_holding

    # History

}
832 = {		#Silverfall

	# Misc
	holding = church_holding

	# History
}

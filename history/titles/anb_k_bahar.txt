k_bahar = {
	1000.1.1 = { change_development_level = 9 }
	998.1.1 = {
		holder = bulwari0001	#Hussam of Nisabat
	}
	1007.5.17 = {
		liege = e_bulwar
	}
}

c_aqatbar = {
	1000.1.1 = { change_development_level = 17 }
	998.1.1 = {
		holder = bulwari0001	#Hussam of Nisabat
	}
}

c_yesdbar = {
	1000.1.1 = { change_development_level = 7 }
}

c_bazibar = {
	1000.1.1 = { change_development_level = 8 }
}

c_eduz_wez = {
	1000.1.1 = { change_development_level = 8 }
	1008.6.21 = {
		holder = sartazuir_0001
		liege = k_bahar
	}
}

c_agselum = {
	1000.1.1 = { change_development_level = 9 }
}

c_deskumar = {
	1000.1.1 = { change_development_level = 11 }
}

c_fajabahar = {
	1000.1.1 = { change_development_level = 8 }
}

c_akal_uak = {
	1000.1.1 = { change_development_level = 13 }
}

c_azka_evran = {
	1000.1.1 = { change_development_level = 11 }
	1018.3.4 = { 
		holder = evranzuir_0001 #evran evranzuir
		liege = "k_bahar"
	}
}

c_thissilen = {
	1000.1.1 = { change_development_level = 7 }
}

d_thissilen = {
	1000.1.1 = { change_development_level = 7 }
}
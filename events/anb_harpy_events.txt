﻿namespace = anb_harpy_initialization

anb_harpy_initialization.0001 = {
	scope = none
	hidden = yes

	immediate = {
		every_living_character = {
			trigger_event = { on_action = harpy_initialization_pulse }
		}
	}
}

anb_harpy_initialization.0002 = {
	hidden = yes

	trigger = {
		has_trait = race_harpy
		is_male = yes
	}

	immediate = {
		remove_trait = race_harpy
		if = {
			limit = {
				OR = {
					has_culture = culture:nansalyra
					has_culture = culture:heunthulyra
					has_culture = culture:firanyalyra
				}
			}
			set_culture = culture:gelkar
			add_trait = race_human
		}
		else_if = {
			limit = {
				has_culture = culture:siadannira
			}
			set_culture = culture:masnsih
			add_trait = race_human
		}
		else_if = {
			limit = {
				has_culture = culture:stonewing
			}
			set_culture = culture:khasani
			add_trait = race_human
		}
	}
}